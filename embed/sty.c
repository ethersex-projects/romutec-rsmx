.button, .lamps{
  background-color: #333; 
  padding: 5px;
  margin-top: 10px;
  margin-bottom: 10px;

  color: #eee;
  text-decoration: none;
  text-align: center;
  display: block;
}

.small_button {
  color: black;
  text-decoration: none;
}

.lamp_green, .lamp_red, .lamp_yellow, .lamp_dark {
  text-decoration: none;
}

.button:hover, .small_button:hover{
  background-color: #444;
}

.module {
   margin: 10px;
   padding: 10px;
   background-color: #888; 
   text-align: center;
   width: 450px;
 }

#logconsole {
  padding: 5px 5px 5px 5px;
  margin-top: 10px;
  width: 98%;
  border: medium dotted black;
  visibility: hidden;
}

#logconsole .lognotice {
  color: green;
}

#logconsole .logerror {
  color: red;
  font-weight: bold;
}

.inputtable td { border-bottom: 2px dotted #99ff99; padding-left: 10px;}
.inputtable .iolabel { border-right: 2px dotted #99ff99; }
.inputtable .io_lastline td { border-bottom: none; }


#modal {
  background-color: gray;
  position: absolute; 
  top: 0px; 
  left:0px; 
  width: 100%; 
  height:100%;
}
#modal #modal_window {
  background-color: white; 
  position: absolute; 
  top: 25%; 
  left:35%; 
  width: 25%;
  padding:10px;
}
