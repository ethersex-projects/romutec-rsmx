var isIE/*@cc_on = true@*/;

var modules = {
    "10": ["RLL", "XLM10"],
    "20": ["RPK", "XDH10"],
    "30": ["RLK", "XDH12"],
    "40": ["RAG", "XAH10"],
    "50": ["RKK", "XDH11"],
    "e0" : ["RDC", "RDC100"]
};

function returnObjById( id )
{
    if (document.getElementById)
        var returnVar = document.getElementById(id);
    else if (document.all)
        var returnVar = document.all[id];
    else if (document.layers)
	var returnVar = document.layers[id];
    return returnVar;
}
function _(id, value) {
    var obj = $(id);
    if (obj) obj.innerHTML = value;
}

function signed(value) {
    if (value & 0x8000) return value - 0xffff - 1;
    return value;
}

var ArrAjax = new Object();
ArrAjax.aufruf = function (address, handler, method, data, timeout)
{
    var xmlHttp = null;
    // Mozilla, Opera, Safari sowie Internet Explorer 7
    if (typeof XMLHttpRequest != 'undefined') {
        xmlHttp = new XMLHttpRequest();
    }
    if (!xmlHttp) {
        // Internet Explorer 6 und älter
        try {
            xmlHttp  = new ActiveXObject("Msxml2.XMLHTTP");
        } catch(e) {
            try {
                xmlHttp  = new
                ActiveXObject("Microsoft.XMLHTTP");
            } catch(e) {
                xmlHttp  = null;
            }
        }
    }
    if (!xmlHttp) {
        alert('No Ajax support possible');
        throw Exception("No ajax support");
        return false;
    }
    xmlHttp.url_ = address;
    xmlHttp.open(method, address, true);
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4) {
            modbus_command_handler(xmlHttp, handler, data);
            window.clearTimeout(xmlHttp.timeoutId);
        }
    };
    xmlHttp.send(null);
    if (timeout) {
        xmlHttp.timeoutId = setTimeout(function() {
                xmlHttp.abort();
            }, timeout);
    }

    return xmlHttp;
};


    var ModbusQueue = new Array();
var ModbusQueueLock = new Object();
ModbusQueueLock.locked = false;
var ModbusEmptyCallbacks = new Array();

function modbus_command(command, handler, data, timeout, retry) {
    var url = '/ecmd?mb recv ' + command;
    var request = new Object();
    request.command = command;
    request.handler = handler;
    request.data = data;
    request.timeout = timeout;


    if (ModbusQueueLock.locked) {
        if (ModbusQueue.length > 20) return;
        ModbusQueue.push(request);
    } else {
        if ( retry != undefined)
            request.retry = retry;
        else {
            request.retry = 4;
        }
        ArrAjax.aufruf(url, handler, 'GET', request, timeout);
        ModbusQueueLock.locked = true;
    }
    var loader = returnObjById('loader');
    if(loader) loader.style.visibility = "visible";
}

function modbus_empty_callback(cb, data) {
    if (ModbusQueue.length == 0) {
        cb(data);
        return;
    }
    ModbusEmptyCallbacks.push(new Array(cb, data));
}

function modbus_command_handler(request, handler, data) {
    var answer = new Object();
    answer.STATUS_ERROR = 1;
    answer.STATUS_OK = 0;

    if (request.readyState != 4 || request.status != 200) {
        answer.status = answer.STATUS_ERROR;
        answer.error_message = "network error";
    } else if (request.responseText.substr(0, 13) == 'modbus error:'){
        if (data.retry == undefined) data.retry = 0;
        if (data.retry > 0) {
            /* We retry the request */
            ModbusQueueLock.locked = false;
            modbus_command(data.command, data.handler, data.data, data.timeout, data.retry - 1);;
            return;
        } else {
            answer.status = answer.STATUS_ERROR;
            answer.error_message = request.responseText.substr(13, 100);
        }
    } else {
        answer.status = answer.STATUS_OK;
        answer.message = request.responseText;
        answer.client = parseInt(answer.message.substr(0,2), 16);
        answer.command = parseInt(answer.message.substr(2,2), 16);
        if (answer.command == 6) {
            answer.address = parseInt(answer.message.substr(4,4), 16);
            answer.value = parseInt(answer.message.substr(8,4), 16);
        } else if (answer.command == 3 || answer.command == 4) {
            answer.length = parseInt(answer.message.substr(4, 2), 16);
            answer.values = new Array();
            for (var i = 0; i < (answer.length/2); i++) {
                answer.values.push(parseInt(answer.message.substr(6 + i*4, 4), 16));
            }
        } else if (answer.command == 7) { /* Device Name */
            answer.length = parseInt(answer.message.substr(4, 2), 16);
            answer.name = "";
            for (var i = 0; i < answer.length - 1; i++) {
                answer.name += String.fromCharCode(parseInt(answer.message.substr(6 + i*2, 2), 16));
            }
        }
    }
    if (handler) {
        handler(answer, data.data);
    }
    if (ModbusQueue.length > 0) {
        window.setTimeout("var next_command = ModbusQueue.shift();"
                          + "ModbusQueueLock.locked = false;"
                          + "modbus_command(next_command.command, next_command.handler, next_command.data, next_command.timeout);",50);
    } else {

        ModbusQueueLock.locked = false;
        for (var i = 0; i < ModbusEmptyCallbacks.length; i++)  {
            ModbusEmptyCallbacks[i][0](ModbusEmptyCallbacks[i][1]);
        }
        ModbusEmptyCallbacks = new Array();
        var loader = returnObjById('loader');
        if (loader) loader.style.visibility = "hidden";

    }
}

function modbus_set_name(address, name) {
    if (!name) return;
    var new_name = name.substr(0, 16);
    if (name.length > new_name.length) alert("Name wurde auf '" + new_name + "' gek&uuml;rzt");

    var message = byte2hex(address) + "07" + byte2hex(new_name.length + 1);

    for (var i = 0; i < new_name.length; i++) {
        if (new_name.charCodeAt(i) > 255) continue;
        message += byte2hex(new_name.charCodeAt(i));
    }
    message += "00";

    modbus_command(message);
}

// Led blinking
var blinking_leds = new Array();

function red_blink() {
    if (blinking_leds.length == 0) return;
    var color = blinking_leds[0].style.backgroundColor;
    for (var i = 0; i < blinking_leds.length; i++) {
        var lamp = blinking_leds[i];
        if (color == "red") {
            SetLampColor(lamp,"dark")
                } else {
            SetLampColor(lamp,"red")
                }
    }
}
setInterval("red_blink()", 500);

function byte2hex(to_convert) {
    var hex = to_convert.toString(16);
    if (hex.length == 1)
        return '0'+hex;
    else
        return hex;
}
//logging
function log_get_lines() {
    return returnObjById('logconsole').getElementsByTagName('div').length;
}

function SetLampColor(lamp, color) {
    lamp.setAttribute(isIE ? "className" : "class", "lamp_" + color);
    if (color == 'dark')
        lamp.style.backgroundColor = '#666';
    else
        lamp.style.backgroundColor = color;
}

function log_clean() {
    var loglines = 16;
    if ( log_get_lines() > loglines ) {
        var logconsole = returnObjById('logconsole');
        var nodes = logconsole.getElementsByTagName('div');
        while (nodes.length > loglines) {
            logconsole.removeChild(nodes[loglines]);
        }
    }
}

function logger(code, text) {
    var logconsole = returnObjById('logconsole');
    logconsole.style.visibility = "visible";


    var div = document.createElement("div");

    var jetzt = new Date();
    var Std = jetzt.getHours();
    var Min = jetzt.getMinutes();
    var StdAusgabe = ((Std < 10) ? "0" + Std : Std);
    var MinAusgabe = ((Min < 10) ? "0" + Min : Min);
    var text = StdAusgabe + ":" + MinAusgabe + " " + text;

    var neuText = document.createElement("div");
    if (code == '0') {
        neuText.setAttribute(isIE ? "className" : "class", "lognotice");
        neuText.innerHTML = text;
    } else {
        neuText.setAttribute(isIE ? "className" : "class", "logerror");
        neuText.innerHTML = 'Fehler: ' + text;
    }
    logconsole.insertBefore(neuText, logconsole.firstChild);
    log_clean();
}

function show_modal() {
    var obj = returnObjById("modal");
    obj.style.display = 'inline';
}

function hide_modal() {
    var obj = returnObjById("modal");
    obj.style.display = 'none';
}

function goto_modal() {
    var new_address;
    var num = parseInt(returnObjById("module_number").value);
    var addr = parseInt(returnObjById("module_address").value);
    if (!isNaN(addr) && addr >= 0 && addr <= 255) {
        new_address = addr;
    } else if (!isNaN(num) && num >= 0 && num <=15) {
        new_address = num | parseInt(returnObjById("module_type").value, 16);
    } else {
        var obj = returnObjById("addr_fehler");
        obj.style.display = "inline";
    }
    goto_module(new_address);
}

function goto_module(addr, target) {
    if (!target)
        target = "_self";
    if (target == "new") target = undefined;

    for (var id in modules) {
        if ((addr & parseInt("f0", 16)) == parseInt(id, 16)) {
            window.open("/" +modules[id][0].toLowerCase() +".ht?id="+addr, target);
            return;
        }
    }
    window.open("/idx.ht", target);
}

function generate_footer() {
    var footer = returnObjById('footer');
    var options = "";
    for (var id in modules) {
        options += "<option value='" + id + "'>" + modules[id][1] + "</option>";
    }
    footer.innerHTML = '<div id=logconsole></div> <div id=modal style="display: none"> <div id=modal_window> <h2>Anderes Modul</h2> <table border=1 cellspacing=0 style="width:100%;"><tr><td>Typ</td><td>Nummer (0-15)</td></tr> <tr><td><select id="module_type" size="1">' +options + '</select></td><td> <input onchange="javascript:goto_modal()" id="module_number" size=8 /></td></tr></table> <hr/> <p>Direkteingabe (0-255)<br/> <input onchange="javascript:goto_modal()" id="module_address"/></p> <div id="addr_fehler" style="display: none; background-color: red;">Fehlerhafte Eingabe</div> <table><tr> <td><a href="javascript:goto_modal()" class=button>anzeigen</a></td> <td><a href="javascript:hide_modal()" class=button>abbrechen</a></td> </tr></table> </div> </div>';
}

function generate_header(no_module) {
    var header = returnObjById('header');
    var heading;
    if (!no_module)
	heading = "<h3><img src='la.gif' id='loader' style='visibility: hidden'>&nbsp;Status von " + address + " (0x" +
        byte2hex(address).toUpperCase()+")<span id=module_name></span></h3>";

    var innerHTML = '<div><a href="/">Zur&uuml;ck</a> | <a href="javascript:show_modal();">Anderes Modul anzeigen</a>';
    if (!no_module)
	innerHTML += ' | <a href="javascript:module_change_name()">Modulname &auml;ndern</a></div><h3 id=heading>' + heading + '</h3>';

    header.innerHTML += innerHTML;
}

function name_handler(answer, data){
    var obj = returnObjById("module_name");
    if (answer.name && answer.name.length > 0) {
        obj.innerHTML = ": " +answer.name;
    } else {
        obj.innerHTML = "";
    }
}

function module_change_name() {
    modbus_set_name(address, window.prompt("Neuer Name des Moduls "
                                           + address + " (0x" + byte2hex(address) + ")"));
    modbus_command(byte2hex(address) + '0700', name_handler);
}

