/* vim:fdm=marker ts=4 et ai
 * {{{
 *
 * Copyright (c) 2008 by Christian Dietrich <stettberger@dokucode.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * For more information on the GPL, please go to:
 * http://www.gnu.org/copyleft/gpl.html
 }}} */

#include "config.h"
#include "rsmx_logic/rsmx_logic.h"

#define MODBUS_HOLD_REGISTERS 11
#define MODBUS_INPUT_REGISTERS 16

// Common registers
#define MODBUS_VERSION 0
#define MODBUS_LAMP_TEST 1
#define MODBUS_SEND_FORCE 2
#define MODBUS_NEW_ERROR 3
#define MODBUS_ERROR 4

#ifdef MODBUS_CLIENT_SUPPORT

uint16_t error_buffer[16];
uint8_t l_timer;

uint16_t
modbus_read_holding(uint8_t address) 
{
  if (address == MODBUS_VERSION)
    return 1;
  if (address == MODBUS_SEND_FORCE)
    return rsmx_logic_get_state() == RSMX_UNACKED_ERROR ? 1 : 0;
  if (address == MODBUS_NEW_ERROR)
    return rsmx_logic_get_state() == RSMX_NEW_ERROR ? 1 : 0;
  if (address == MODBUS_ERROR)
    return rsmx_logic_get_state() == RSMX_OLD_ERROR ? 1 : 0;
  return 0;
}

void
modbus_write_holding(uint8_t address, uint16_t value) 
{
  if (address == MODBUS_LAMP_TEST && value != 0) {
    l_timer = value;
  } else if (address == MODBUS_SEND_FORCE && value != 0 
             && rsmx_logic_get_state() != RSMX_UNACKED_ERROR) {
      rsmx_logic_set_state(RSMX_UNACKED_ERROR); 
      if ((error_buffer[0] & 0xff) != value) {
          memmove(&error_buffer[1], &error_buffer[0], sizeof(error_buffer) - 2);
          error_buffer[0] = value | 0x400;
      }
  } else if (address == MODBUS_NEW_ERROR && value == 0) {
      for (uint8_t i = 0; i< 16; i++)
	  error_buffer[i] &= ~0x400;
      rsmx_logic_error_ack();
  } else if (address == MODBUS_ERROR && value == 0) {
      rsmx_logic_reset();
      memset(error_buffer, 0, sizeof(error_buffer));
  }
}

uint16_t
modbus_read_input(uint8_t address) 
{
  return error_buffer[address];
}
#endif
