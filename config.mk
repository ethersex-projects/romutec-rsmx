PACK=../../burner/pack

load: ethersex.hex
	avrdude -p $(MCU) -c avrispmkII -P usb -U flash:w:ethersex.hex -B 1

fuse:
	avrdude -p $(MCU) -c avrispmkII -P usb -U lfuse:w:0xff:m -U hfuse:w:0xd9:m -U efuse:w:0xfc:m -B 1000

pack: 
	for var in $$(ls variant-*); do \
	   cp $$var .config; \
	   echo  "\t\n\n" | make menuconfig; \
	   make -j3 ethersex.hex; \
	   $(PACK)  --flash=ethersex.hex --low=0xff --high=0xd9 --extended=0xfc \
			--sck=1 --part=$(MCU) --name="XBZ1010 - $(shell date) - $$var" \
	    	../images/"XBZ1010_$(shell date +%Y%m%d_%H%M)-$$var".project; \
	done
