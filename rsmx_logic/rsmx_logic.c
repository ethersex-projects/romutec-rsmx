/* vim:fdm=marker et ai
 * {{{
 *
 * Copyright (c) 2007 by Christian Dietrich <stettberger@dokucode.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * For more information on the GPL, please go to:
 * http://www.gnu.org/copyleft/gpl.html
 }}} */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include "core/bit-macros.h"
#include "core/eeprom.h"
#include "config.h"
#include "protocols/modbus/modbus_client.h"
#include "protocols/modbus/modbus_state.h"
#include "protocols/modbus/modbus_net.h"
#include "protocols/modbus/modbus.h"
#include "rsmx_logic.h"

volatile uint8_t module_status;
uint8_t msg_to_send[16];
int16_t dummy_recv;

static uint8_t door_inputs;
static uint8_t door_available = 0;;
static uint8_t door_recv_timer = 0;;

uint8_t blink_timer;
uint8_t send_timer;
uint8_t send_status;
uint8_t entsperr_timer;
uint8_t rsmx_packet_received;
extern uint8_t l_timer;

#define USE_USART 1
#define BAUD 19200
#include "core/usart.h"

extern uint16_t error_buffer[16];
extern uint8_t modbus_recv_timer;
extern int16_t *modbus_recv_len_ptr;
extern struct modbus_buffer modbus_data;

#ifdef RSMX_POLL_SUPPORT
uint8_t rsmx_request_addr;
uint8_t rsmx_client_map[32]; // 256 Bit for 256 clients
extern struct modbus_connection_state_t modbus_client_state;
#endif

uint8_t
CRC8 (uint8_t input, uint8_t seed)
{
    uint8_t i, feedback;
    for (i = 0; i < 8; i++)
        {
            feedback = ((seed ^ input) & 0x01);
            if (!feedback)
                seed >>= 1;
            else
                {
                    seed ^= 0x18;
                    seed >>= 1;
                    seed |= 0x80;
                }
            input >>= 1;
        }
    return seed;
}

/* We generate our own usart init module, for our usart port */
generate_usart_init()

void
rsmx_logic_init(void)
{
    module_status = RSMX_OK;
    DDR_CONFIG_OUT(STATUS_RED);
    DDR_CONFIG_OUT(STATUS_GREEN);

    DDR_CONFIG_OUT(RSM_TX);
    PIN_CLEAR(RSM_TX);

    /* Initialize the usart module */
    usart_init();

#ifdef RSMX_POLL_SUPPORT
    rsmx_request_addr = MODBUS_BROADCAST;
#endif

}

void
rsmx_logic_outputs(uint16_t leds)
{

    /* Some pins must be used inverted */
    leds ^= RSMX_LED_RPK | RSMX_LED_RLL | RSMX_LED_RLK | RSMX_LED_RAG
        | RSMX_LED_FREE;

#define LED_OUTPUT(name, in) if ((in) & ( RSMX_LED_ ## name )) PIN_SET(STATUS_ ## name); \
    else PIN_CLEAR(STATUS_ ## name);

    PORTA = leds & 0xff;

    LED_OUTPUT(GREEN, leds);
    LED_OUTPUT(RED, leds);
}

#ifdef RSMX_POLL_SUPPORT
uint8_t rsmx_get_next_poll(void) {
    static uint8_t full_scan = 0, actual_scan, normal_op;
    uint8_t ret;

    normal_op++;

    if (normal_op < 2) {
        uint8_t present = 0;
        while (present == 0 && actual_scan < 240) {
            actual_scan ++;
            present = rsmx_client_map[actual_scan >> 3] & _BV(actual_scan & 0x7);
        }

        if (actual_scan >= 240) {
            actual_scan = 0;
            goto full_scan_next;
        }

        ret = actual_scan;
    } else {
        normal_op = 0;
    full_scan_next:
        full_scan ++;
        if (full_scan >= 240)
            full_scan = 0;
        ret = full_scan;
    }
    return ret;
}
#endif

uint8_t lfsr[8];
uint8_t prng(uint8_t seed) {
    uint8_t tmp = seed ^ lfsr[7];
    tmp += CRC8(lfsr[3], lfsr[2]) + CRC8(lfsr[5], lfsr[6]);
    tmp += CRC8(seed ^ tmp, lfsr[4]);
    memmove(lfsr + 1, lfsr, sizeof(lfsr)-1);
    lfsr[0] = tmp;
    return tmp;
}

void
rsmx_logic_periodic(void)
{
    uint16_t outputs = 0;

    if (l_timer) {
        if (l_timer > 1)
            l_timer -= 2;
        else if (l_timer)
            l_timer--;
    }
    if (door_recv_timer) door_recv_timer--;

    /* Send status to fron module */
    if (send_status) {
        send_status = 0;

        PIN_SET(RSM_TX);
        usart(UDR) = 0xff;
        usart(UCSR,A) |= _BV(usart(TXC));
        while ( !( usart(UCSR,A) & _BV(usart(TXC))) );

        uint8_t data = rsmx_logic_get_state() | (l_timer ? 0x80 : 0) | (PIN_HIGH(INPUT_FREE) ? 0 : 0x40);
        usart(UDR) = data;
        usart(UCSR,A) |= _BV(usart(TXC));
        while ( !( usart(UCSR,A) & _BV(usart(TXC))) );

        usart(UDR) = CRC8(data, 0);
        usart(UCSR,A) |= _BV(usart(TXC));
        while ( !( usart(UCSR,A) & _BV(usart(TXC))) );
        PIN_CLEAR(RSM_TX);
    }


    union ModbusRTU *rtu = (void *)msg_to_send;
    /* Door input processing */
    send_timer++;
    if (send_timer > 10)
        send_timer = 0;

    if (!send_timer) {
        if (door_inputs & 0x04 || !PIN_HIGH(RESET)) { /* Quitieren */
            /* A new error occured, we have to send a shutup to the slave modules */
            rtu->write.addr = MODBUS_BROADCAST;
            rtu->write.cmd = MODBUS_CMD_WRITE_HOLDING;
            rtu->write.ptr = HTONS(4);
            rtu->write.value = HTONS(0);
            entsperr_timer = 50;

            dummy_recv = 0;
            modbus_rxstart(msg_to_send, 6, &dummy_recv);
        } else if (door_inputs & 0x01 || !PIN_HIGH(LAMPENTEST)) { /* Lampentest */
            /* A new error occured, we have to send a shutup to the slave modules */
            rtu->write.addr = MODBUS_BROADCAST;
            rtu->write.cmd = MODBUS_CMD_WRITE_HOLDING;
            rtu->write.ptr = HTONS(1);
            rtu->write.value = HTONS(0xff);

            dummy_recv = 0;
            modbus_rxstart(msg_to_send, 6, &dummy_recv);
        } else if (door_inputs & 0x02 || !PIN_HIGH(QUITIEREN)) { /* Quitieren */
            /* A new error occured, we have to send a shutup to the slave modules */
            rtu->write.addr = MODBUS_BROADCAST;
            rtu->write.cmd = MODBUS_CMD_WRITE_HOLDING;
            rtu->write.ptr = HTONS(3);
            rtu->write.value = HTONS(0);

            dummy_recv = 0;
            modbus_rxstart(msg_to_send, 6, &dummy_recv);
        } else if (module_status == RSMX_UNACKED_ERROR) {
            /* A new error occured, we have to send a shutup to the slave modules */
            rtu->write.addr = error_buffer[0] & 0xff;
            rtu->write.cmd = MODBUS_CMD_WRITE_HOLDING;
            rtu->write.ptr = HTONS(2);
            rtu->write.value = 0;

            dummy_recv = 0;
            modbus_rxstart(msg_to_send, 6, &dummy_recv);

            module_status = RSMX_NEW_ERROR;
            blink_timer = 0;
        }
    } else if (send_timer == 5) {
#ifdef RSMX_POLL_SUPPORT
        if (! modbus_recv_len_ptr && uip_len == 0) {
            uint8_t addr = rsmx_get_next_poll();
            union ModbusRTU *rtu = (union ModbusRTU *)modbus_client_state.data;
            rtu->read.addr = addr;
            rtu->read.cmd = MODBUS_CMD_READ_HOLDING;
            rtu->read.ptr = HTONS(2); // Send Force
            rtu->read.len = HTONS(1);
            int16_t recv_len = 0;
            modbus_rxstart(modbus_client_state.data, 6, NULL);
            modbus_recv_len_ptr = NULL;
            rsmx_request_addr = addr;
        }
#endif
    }

    blink_timer ++;
    if (blink_timer > 50)
        blink_timer = 0;

    /* Set the outouts according to the state */
    if (door_available && !door_recv_timer) {
        if (blink_timer > 25)
            outputs |= RSMX_LED_GREEN | RSMX_LED_RED;
    } else if (module_status == RSMX_OK) {
        outputs |= RSMX_LED_GREEN;
    } else if (module_status == RSMX_NEW_ERROR 
               || module_status == RSMX_UNACKED_ERROR)  {
        outputs |= RSMX_RELAIS_HUPE | RSMX_RELAIS_SA;
        if (blink_timer > 25)
            outputs |= RSMX_LED_RED;
    } else if (module_status == RSMX_OLD_ERROR){
        outputs |= RSMX_LED_RED | RSMX_RELAIS_SA;
    }
    if (entsperr_timer) {
        outputs &= ~(RSMX_LED_RED);
        outputs |= RSMX_RELAIS_ENTSPERREN;
        entsperr_timer --;
    }

    /* error_buffer.len == 16, do it with the overflow */
    for (uint8_t i = 15; i < 16; i--) {
        uint16_t error = error_buffer[i];
        /* RLL Modul prefix: 0x10 */
        if ((error & 0xf0) == 0x10) {
            if (error & 0x400) {
                if ( ((error & 0x200) && ((blink_timer % 25) < 13))
                     || ((error & 0x100) && (blink_timer < 26)) ) {
                    outputs &= ~RSMX_LED_RLL;
                    continue;
                }
            }
            outputs |= RSMX_LED_RLL;
            /* RPK Modul prefix: 0x20 */
        } else if ((error & 0xf0) == 0x20) {
            if (error & 0x400) {
                if ( ((error & 0x200) && ((blink_timer % 25) < 13))
                     || ((error & 0x100) && (blink_timer < 26)) ) {
                    outputs &= ~RSMX_LED_RPK;
                    continue;
                }
            }
            outputs |= RSMX_LED_RPK;
            /* RLK Modul prefix: 0x30 */
        } else if ((error & 0xf0) == 0x30) {
            if (error & 0x400) {
                if ( ((error & 0x200) && ((blink_timer % 25) < 13))
                     || ((error & 0x100) && (blink_timer < 26)) ) {
                    outputs &= ~RSMX_LED_RLK;
                    continue;
                }
            }
            outputs |= RSMX_LED_RLK;
        } else if ((error & 0xf0) == 0x40) {
            if (error & 0x400) {
                if ( ((error & 0x200) && ((blink_timer % 25) < 13))
                     || ((error & 0x100) && (blink_timer < 26)) ) {
                    outputs &= ~RSMX_LED_RAG;
                    continue;
                }
            }
            outputs |= RSMX_LED_RAG;
        }

    }

    if ((modbus_data.crc_len != 0) || modbus_recv_timer || rsmx_packet_received) {
        rsmx_packet_received = 0;
        outputs |= RSMX_LED_FREE;
    }

    if (l_timer > 0) {
        /* Lampentest */
        outputs |= RSMX_LED_RPK | RSMX_LED_RLL | RSMX_LED_RLK
            | RSMX_LED_GREEN | RSMX_LED_RAG
            | RSMX_LED_FREE | RSMX_LED_GREEN | RSMX_LED_RED;
    }

    rsmx_logic_outputs(outputs);
}

void
rsmx_logic_set_state(uint8_t state)
{
    if (state > module_status) {
        module_status = state;
    }
}

uint8_t
rsmx_logic_get_state(void)
{
    if (module_status == RSMX_UNACKED_ERROR) return RSMX_NEW_ERROR;
    return module_status;
}

void
rsmx_logic_error_ack(void)
{
    if (module_status == RSMX_NEW_ERROR)
        module_status = RSMX_OLD_ERROR;
}

void
rsmx_logic_reset(void)
{
    module_status = RSMX_OK;
    memset(error_buffer, 0, sizeof(error_buffer));
}

uint8_t front_module_buffer[10];
uint8_t front_module_len;

SIGNAL(usart(USART,_RX_vect))
{
    if (usart(UCSR,A) & _BV(usart(FE)) || usart(UCSR,A) & _BV(usart(DOR))) {
        uint8_t tmp = usart(UDR);
        (void) tmp;
    }

    front_module_buffer[front_module_len++] = usart(UDR);

    /* Module address is 0xf0 */
    if (front_module_buffer[0] != 0xf0) {
        front_module_len = 0;
        return;
    }

    /* Get Status */
    if (front_module_len == 4 && front_module_buffer[1] == 0x00) {
        uint8_t i, crc = 0;
        for (i = 0; i < 3; i++) {
            crc = CRC8(front_module_buffer[i], crc);
        }
        if (crc == front_module_buffer[3]) {
            door_inputs = ~(front_module_buffer[2] & 0x07);
            door_available = 1;
            door_recv_timer = 60;
            send_status = 1;
            front_module_len = 0;
        }

    } else if (front_module_len > 4)
        front_module_len = 0;
}

/*
  -- Ethersex META --
  header(rsmx_logic/rsmx_logic.h)
  init(rsmx_logic_init)
  timer(1, rsmx_logic_periodic())
*/


