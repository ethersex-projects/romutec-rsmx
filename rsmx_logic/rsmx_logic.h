/* vim:fdm=marker ts=4 et ai
 * {{{
 *
 * Copyright (c) 2007 by Christian Dietrich <stettberger@dokucode.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * For more information on the GPL, please go to:
 * http://www.gnu.org/copyleft/gpl.html
 }}} */

#ifndef _RSMX_LOGIC_H
#define _RSMX_LOGIC_H

void rsmx_logic_init(void);
void rsmx_logic_periodic(void);
void rsmx_logic_set_state(uint8_t state);
uint8_t rsmx_logic_get_state(void);

void rsmx_logic_error_ack(void);
void rsmx_logic_reset(void);

/* The order is IMPORTANT */
enum {
  RSMX_OK = 0,
  RSMX_OLD_ERROR,
  RSMX_NEW_ERROR,
  RSMX_UNACKED_ERROR,
};

enum {
    RSMX_RELAIS_SA           = 1,
    RSMX_RELAIS_HUPE         = 2,
    RSMX_RELAIS_ENTSPERREN   = 4,
    RSMX_LED_RPK             = 8,
    RSMX_LED_RLL             = 16,
    RSMX_LED_RLK             = 32,
    RSMX_LED_RAG             = 64,
    RSMX_LED_FREE            = 128,
    RSMX_LED_GREEN           = 256,
    RSMX_LED_RED             = 512,
};


#endif /* _RSMX_LOGIC_H */
