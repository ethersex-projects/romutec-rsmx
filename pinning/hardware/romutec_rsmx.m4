dnl
dnl romutec-rsmx.m4
dnl
dnl Pin Configuration for 'romutec-rsmx'.  Edit it to fit your needs.
dnl

/* modbus tx  */
pin(MODBUS_TX, PD5)

/* rsm tx */
pin(RSM_TX, PD4)

/* port the enc28j60 is attached to */
pin(SPI_CS_NET, SPI_CS_HARDWARE)

pin(STATUS_RED, PD6)
pin(STATUS_GREEN, PD7)

pin(STATUS_SA,    PA0, OUTPUT)
pin(STATUS_HUPE,  PA1, OUTPUT)
pin(STATUS_ENTSPERREN, PA2, OUTPUT)
pin(STATUS_RPK,   PA3, OUTPUT)
pin(STATUS_RLL,   PA4, OUTPUT)
pin(STATUS_RLK,   PA5, OUTPUT)
pin(STATUS_RAG,   PA6, OUTPUT)
pin(STATUS_FREE,  PA7, OUTPUT)


pin(LAMPENTEST, PC0)
pin(QUITIEREN, PC1)
pin(RESET, PC2)
pin(INPUT_FREE, PC3)


